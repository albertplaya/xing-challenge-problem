<?php

namespace Xing\Nasa\Acceptance;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class NasaSendsMessageCommandTest extends KernelTestCase
{
    private $application;

    public function setUp()
    {
        $kernel = static::createKernel();
        $this->application = new Application($kernel);
    }

    public function testExecute()
    {
        $command = $this->application->find('app:nasa-sends-message-command');
        $commandTester = new CommandTester($command);
        $commandTester->setInputs([
            '5 5',
            '1 2 N',
            'L M L M L M L M M',
            '3 3 E',
            'M M R M M R M R R M',
            ''
        ]);

        $commandTester->execute(['command' => $command->getName()]);
        $expectedMessage = '1 3 N' . PHP_EOL . '5 1 E' . PHP_EOL . PHP_EOL;
        $this->assertEquals($expectedMessage,  $commandTester->getDisplay());
    }
}