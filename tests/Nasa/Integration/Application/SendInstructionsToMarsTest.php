<?php

namespace Xing\Test\Nasa\Integration\Application;

use PHPUnit\Framework\TestCase;
use Xing\Nasa\Application\SendInstructionsToMars;
use Xing\Nasa\Domain\Message\NasaInputMessage;

class SendInstructionsToMarsTest extends TestCase
{
    public function testSendInstructionsToMarsAndReceiveTheOutputCorrectly()
    {
        $nasaInputMessage = \Mockery::mock(NasaInputMessage::class);
        $nasaInputMessage
            ->shouldReceive('getPlateauCoordinates')
            ->once()
            ->andReturn('5 5');
        $nasaInputMessage
            ->shouldReceive('getRoverInstructions')
            ->once()
            ->andReturn([['1 2 N', 'L M L M L M L M M'],['3 3 E', 'M M R M M R M R R M']]);


        $sut = new SendInstructionsToMars();
        $result = $sut->make($nasaInputMessage);

        $roverPositions = $result->getRoverPositions();
        $this->assertEquals('1 3 N', $roverPositions[0]->getCurrentPositionAsString());
        $this->assertEquals('5 1 E', $roverPositions[1]->getCurrentPositionAsString());
    }
}