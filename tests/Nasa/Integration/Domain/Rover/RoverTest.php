<?php

namespace Xing\Test\Nasa\Integration\Domain;

use PHPUnit\Framework\TestCase;
use Xing\Nasa\Domain\Rover\Instruction;
use Xing\Nasa\Domain\Rover\Rover;

class RoverTest extends TestCase
{
    public function testMakeRoverSuccessfully()
    {
        $position = '1 2 N';
        $this->assertInstanceOf(Rover::class, Rover::make($position));
    }

    public function testMoveRoverToLeftOrientationSuccessfully()
    {
        $position = '1 2 N';
        $rover = Rover::make($position);

        $rover->move(Instruction::make('L'));
        $this->assertSame('1 2 W', $rover->getCurrentPositionAsString());
    }

    public function testMoveRoverToRightOrientationSuccessfully()
    {
        $position = '1 2 W';
        $rover = Rover::make($position);

        $rover->move(Instruction::make('R'));
        $this->assertSame('1 2 N', $rover->getCurrentPositionAsString());
    }

    public function testMoveRoverToNorthSuccessfully()
    {
        $position = '1 2 N';
        $rover = Rover::make($position);

        $rover->move(Instruction::make('M'));
        $this->assertSame('1 3 N', $rover->getCurrentPositionAsString());
    }
}