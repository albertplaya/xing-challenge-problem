<?php

namespace Xing\Test\Nasa\Integration\Domain;

use PHPUnit\Framework\TestCase;
use Xing\Nasa\Domain\Plateau\Plateau;
use Xing\Nasa\Domain\Rover\Rover;

class PlateauTest extends TestCase
{
    public function testPlateauMoveOneRoverSuccessfully()
    {
        $plateau = Plateau::make('5 5');
        $plateau->addRover(Rover::make('1 2 N'));
        $plateau->moveRoversSequentially([['L', 'M', 'L', 'M', 'L', 'M', 'L', 'M', 'M']]);
        $this->assertSame('1 3 N', $plateau->getRovers()[0]->getCurrentPositionAsString());
    }

    public function testPlateauMoveAnotherRoverSuccessfully()
    {
        $plateau = Plateau::make('5 5');
        $plateau->addRover(Rover::make('3 3 E'));
        $plateau->moveRoversSequentially([['M', 'M', 'R', 'M', 'M', 'R', 'M', 'R', 'R', 'M']]);
        $this->assertSame('5 1 E', $plateau->getRovers()[0]->getCurrentPositionAsString());
    }

    public function testPlateauMoveSomeRoversSuccessfully()
    {
        $plateau = Plateau::make('5 5');
        $plateau->addRover(Rover::make('1 2 N'));
        $plateau->addRover(Rover::make('3 3 E'));

        $roverInstructions = [
            ['L', 'M', 'L', 'M', 'L', 'M', 'L', 'M', 'M'],
            ['M', 'M', 'R', 'M', 'M', 'R', 'M', 'R', 'R', 'M'],
        ];
        $plateau->moveRoversSequentially($roverInstructions);

        $this->assertSame('1 3 N', $plateau->getRovers()[0]->getCurrentPositionAsString());
        $this->assertSame('5 1 E', $plateau->getRovers()[1]->getCurrentPositionAsString());
    }
}