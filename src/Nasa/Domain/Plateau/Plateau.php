<?php

namespace Xing\Nasa\Domain\Plateau;

use Xing\Nasa\Domain\Rover\Instruction;
use Xing\Nasa\Domain\Rover\Rover;

class Plateau
{
    private $coordinates;
    private $rovers;

    private function __construct(string $coordinates)
    {
        $this->coordinates = $coordinates;
    }

    public static function make(string $coordinates): Plateau
    {
        return new self($coordinates);
    }

    public function addRover(Rover $rover)
    {
        $this->rovers[] = $rover;
    }

    public function moveRoversSequentially(array $instructions)
    {
        $index = 0;
        foreach ($instructions as $instructionsPerRover)
        {
            foreach ($instructionsPerRover as $instruction)
            {
                $this->rovers[$index]->move(Instruction::make($instruction));
            }
            $index++;
        }
    }

    public function getRovers(): array
    {
        return $this->rovers;
    }
}