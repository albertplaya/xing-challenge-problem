<?php

namespace Xing\Nasa\Domain\Message;

class NasaFilledInputMessage implements NasaInputMessage
{
    protected $plateauCoordinates;
    protected $roverInstructions;

    public function addPlateauCoordinates(string $coordinates): void
    {
        $this->plateauCoordinates = $coordinates;
    }

    public function addRoverInstructions(string $position, string $instructions): void
    {
        $this->roverInstructions[] = [$position, $instructions];
    }

    public function getPlateauCoordinates(): string
    {
        return $this->plateauCoordinates;
    }

    public function getRoverInstructions(): array
    {
        return $this->roverInstructions;
    }

}