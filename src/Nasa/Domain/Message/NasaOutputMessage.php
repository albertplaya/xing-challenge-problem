<?php

namespace Xing\Nasa\Domain\Message;

interface NasaOutputMessage
{
    public static function make(array $orientation): NasaOutputMessage;

    public function getRoverPositions(): array;

    public function toString(): string;
}