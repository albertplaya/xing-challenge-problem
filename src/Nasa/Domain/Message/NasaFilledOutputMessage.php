<?php

namespace Xing\Nasa\Domain\Message;

class NasaFilledOutputMessage implements NasaOutputMessage
{

    protected $rovers;

    protected function __construct(array $rovers)
    {
        $this->rovers = $rovers;
    }

    public static function make(array $rovers): NasaOutputMessage
    {
        return new self($rovers);
    }

    public function getRoverPositions(): array
    {
        return $this->rovers;
    }

    public function toString(): string
    {
        $string = '';
        foreach ($this->rovers as $rover) {
            $string .= $rover->getCurrentPositionAsString() . PHP_EOL;
        }
        return $string;
    }


}