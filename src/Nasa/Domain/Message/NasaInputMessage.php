<?php

namespace Xing\Nasa\Domain\Message;

interface NasaInputMessage
{
    public function addPlateauCoordinates(string $coordinates): void;

    public function addRoverInstructions(string $position, string $instructions): void;

    public function getPlateauCoordinates(): string;

    public function getRoverInstructions(): array;
}