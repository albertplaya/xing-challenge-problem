<?php

namespace Xing\Nasa\Domain\Rover;

final class CardinalPoint
{
    private $value;
    CONST North = 'N';
    CONST South = 'S';
    CONST East = 'E';
    CONST West = 'W';

    private function __construct(string $value)
    {
        $this->value = $value;
    }

    public static function makeFromString(string $cardinalPoint): CardinalPoint
    {
        if ($cardinalPoint == self::North) {
            return self::makeNorth();
        } elseif ($cardinalPoint == self::South) {
            return self::makeSouth();
        } elseif ($cardinalPoint == self::East) {
            return self::makeEast();
        } elseif ($cardinalPoint == self::West) {
            return self::makeWest();
        }
    }

    public static function makeWest(): CardinalPoint
    {
        return new self(self::West);
    }

    public static function makeNorth(): CardinalPoint
    {
        return new self(self::North);
    }

    public static function makeEast(): CardinalPoint
    {
        return new self(self::East);
    }

    public static function makeSouth(): CardinalPoint
    {
        return new self(self::South);
    }

    public function value(): string
    {
        return $this->value;
    }

    public function isNorth(): bool
    {
        return (self::North == $this->value);
    }

    public function isSouth(): bool
    {
        return (self::South == $this->value);
    }

    public function isEast(): bool
    {
        return (self::East == $this->value);
    }

    public function isWest(): bool
    {
        return (self::West == $this->value);
    }

    public static function isValid(string $value): bool
    {
        //TO DO
        return true;
    }

    public function toString(): string
    {
        return $this->value;
    }
}