<?php

namespace Xing\Nasa\Domain\Rover;

use Ramsey\Uuid\Uuid;

class Rover
{
    private $uuid;
    private $position;

    private function __construct(Position $position)
    {
        $this->uuid = Uuid::uuid4();
        $this->position = $position;
    }

    public static function make(string $position): Rover
    {
        return new self(Position::make($position));
    }

    public function move(Instruction $instruction)
    {
        $this->position->move($instruction);
    }

    public function getCurrentPositionAsString(): string
    {
        return $this->position->toString();
    }
}