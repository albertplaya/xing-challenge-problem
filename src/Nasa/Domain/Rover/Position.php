<?php

namespace Xing\Nasa\Domain\Rover;

class Position
{
    private $coordinates;
    private $orientation;

    private function __construct(Coordinates $coordinates, Orientation $orientation)
    {
        $this->coordinates = $coordinates;
        $this->orientation = $orientation;
    }

    public static function make(string $position): Position
    {
        $positionIntoParts = explode(' ', $position);
        return new self(Coordinates::make($positionIntoParts[0], $positionIntoParts[1]), Orientation::make($positionIntoParts[2]));
    }

    public function move(Instruction $instruction): void
    {
        if ($instruction->isOrientation()) {
            $this->moveOrientation($instruction);
        } elseif($instruction->isForward()){
            $this->moveForward();
        }
    }

    private function moveOrientation(Instruction $instruction): void
    {
        switch ($instruction->value()) {
            case 'L':
                $this->orientation->moveLeft();
                break;

            case 'R':
                $this->orientation->moveRight();
                break;
        }
    }

    private function moveForward(): void
    {
        $this->coordinates->moveForward($this->orientation);
    }

    public function toString(): string
    {
        return $this->coordinates->toString() . ' ' . $this->orientation->toString();
    }
}