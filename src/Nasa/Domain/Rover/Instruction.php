<?php

namespace Xing\Nasa\Domain\Rover;

class Instruction
{
    private $value;

    private function __construct(string $instruction)
    {
        $this->value = $instruction;
    }

    public static function make(string $instruction): Instruction
    {
        return new self($instruction);
    }

    public function value(): string
    {
        return $this->value;
    }

    public function isOrientation(): bool
    {
        return (in_array($this->value, ['L', 'R']));
    }

    public function isForward(): bool
    {
        return ('M' == $this->value);
    }
}