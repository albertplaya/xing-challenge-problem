<?php

namespace Xing\Nasa\Domain\Rover;

class Orientation
{
    private $cardinalPoint;

    private function __construct(CardinalPoint $cardinalPoint)
    {
        $this->cardinalPoint = $cardinalPoint;
    }

    public static function make(string $cardinalPoint): Orientation
    {
        return new self(CardinalPoint::makeFromString($cardinalPoint));
    }

    public function moveLeft(): void
    {
        if ($this->cardinalPoint->isNorth()) {
            $this->cardinalPoint = CardinalPoint::makeWest();
        } elseif ($this->cardinalPoint->isWest()) {
            $this->cardinalPoint = CardinalPoint::makeSouth();
        } elseif ($this->cardinalPoint->isSouth()) {
            $this->cardinalPoint = CardinalPoint::makeEast();
        } elseif ($this->cardinalPoint->isEast()) {
            $this->cardinalPoint = CardinalPoint::makeNorth();
        }
    }

    public function moveRight(): void
    {
        if ($this->cardinalPoint->isNorth()) {
            $this->cardinalPoint = CardinalPoint::makeEast();
        } elseif ($this->cardinalPoint->isEast()) {
            $this->cardinalPoint = CardinalPoint::makeSouth();
        } elseif ($this->cardinalPoint->isSouth()) {
            $this->cardinalPoint = CardinalPoint::makeWest();
        } elseif ($this->cardinalPoint->isWest()) {
            $this->cardinalPoint = CardinalPoint::makeNorth();
        }
    }

    public function value(): CardinalPoint
    {
        return $this->cardinalPoint;
    }

    public function toString(): string
    {
        return $this->cardinalPoint->toString();
    }
}