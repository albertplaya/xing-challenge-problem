<?php

namespace Xing\Nasa\Domain\Rover;

class Coordinates
{
    private $pointX;
    private $pointY;

    private function __construct(int $pointX, int $pointY)
    {
        $this->pointX = $pointX;
        $this->pointY = $pointY;
    }

    public static function makeEmpty(): Coordinates
    {
        return new self(0, 0);
    }

    public static function make(string $pointX, string $pointY): Coordinates
    {
        return new self(intval($pointX), intval($pointY));
    }

    public function moveForward(Orientation $orientation)
    {
        $cardinalPoint = $orientation->value();
        if ($cardinalPoint->isNorth()) {
            $this->moveUp();
        } elseif ($cardinalPoint->isSouth()) {
            $this->moveDown();
        } elseif ($cardinalPoint->isEast()) {
            $this->moveRight();
        } elseif ($cardinalPoint->isWest()) {
            $this->moveLeft();
        }
    }

    public function toString(): string
    {
        return $this->pointX . ' ' . $this->pointY;
    }

    public function moveUp(): void
    {
        $this->pointY += 1;
    }

    public function moveDown(): void
    {
        $this->pointY -= 1;
    }

    public function moveRight(): void
    {
        $this->pointX += 1;
    }

    public function moveLeft(): void
    {
        $this->pointX -= 1;
    }
}