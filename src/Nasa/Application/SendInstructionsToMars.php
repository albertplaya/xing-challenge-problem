<?php

namespace Xing\Nasa\Application;

use Xing\Nasa\Domain\Message\NasaFilledOutputMessage;
use Xing\Nasa\Domain\Message\NasaInputMessage;
use Xing\Nasa\Domain\Message\NasaOutputMessage;
use Xing\Nasa\Domain\Plateau\Plateau;
use Xing\Nasa\Domain\Rover\Rover;

class SendInstructionsToMars
{
    public function make(NasaInputMessage $message): NasaOutputMessage
    {
        $instructions = [];
        $plateau = Plateau::make($message->getPlateauCoordinates());
        foreach ($message->getRoverInstructions() as $roverInstruction)
        {
            $plateau->addRover(Rover::make($roverInstruction[0]));
            $instructions[] = explode(' ', $roverInstruction[1]);
        }
        $plateau->moveRoversSequentially($instructions);
        return NasaFilledOutputMessage::make($plateau->getRovers());
    }
}
