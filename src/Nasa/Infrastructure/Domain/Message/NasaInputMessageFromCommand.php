<?php

namespace Xing\Nasa\Infrastructure\Domain\Message;

use Xing\Nasa\Domain\Message\NasaInputMessage;

class NasaInputMessageFromCommand implements NasaInputMessage
{
    protected $plateauCoordinates;
    protected $roverInstructions;

    public function __construct()
    {
        $this->plateauCoordinates = '';
        $this->roverInstructions = array();
    }

    public static function make(): self
    {
        return new self();
    }

    public function addPlateauCoordinates(string $coordinates): void
    {
        $this->plateauCoordinates = $coordinates;
    }

    public function addRoverInstructions(string $position, string $instructions): void
    {
        $this->roverInstructions[] = array($position, $instructions);
    }

    public function getPlateauCoordinates(): string
    {
       return $this->plateauCoordinates;
    }

    public function getRoverInstructions(): array
    {
        return $this->roverInstructions;
    }
}