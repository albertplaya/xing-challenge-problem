<?php

namespace Xing\Nasa\Infrastructure\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Xing\Nasa\Application\SendInstructionsToMars;
use Xing\Nasa\Domain\Message\NasaFilledInputMessage;
use Xing\Nasa\Domain\Message\NasaInputMessage;
use Xing\Nasa\Domain\Message\NasaOutputMessage;

class NasaSendsMessageCommand extends Command
{
    protected static $defaultName = 'app:nasa-sends-message-command';

    protected function execute(InputInterface $inputCommand, OutputInterface $outputCommand)
    {
        $helper = $this->getHelper('question');

        $input = new NasaFilledInputMessage();
        $this->askPlateauCoordinates($inputCommand, $outputCommand, $helper, $input);
        $this->askRoverInstructions($inputCommand, $outputCommand, $helper, $input);

        $applicationService = new SendInstructionsToMars();
        $result = $applicationService->make($input);
        $this->printOutput($result, $outputCommand);
    }

    private function askPlateauCoordinates(InputInterface $inputCommand, OutputInterface $outputCommand, QuestionHelper $helper, NasaInputMessage $input): void
    {
        $coordinates = $helper->ask($inputCommand, $outputCommand, new Question(''));
        $input->addPlateauCoordinates($coordinates);
    }

    private function askRoverInstructions(InputInterface $inputCommand, OutputInterface $outputCommand, QuestionHelper $helper, NasaInputMessage $input): void
    {
        while (!empty($position = $helper->ask($inputCommand, $outputCommand, new Question('')))) {
            $instructions = $helper->ask($inputCommand, $outputCommand, new Question(''));
            $input->addRoverInstructions($position, $instructions);
        }
    }

    private function printOutput(NasaOutputMessage $result, OutputInterface $output): void
    {
        $output->writeln($result->toString());
    }
}