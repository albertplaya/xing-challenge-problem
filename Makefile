.PHONY: up
up:
	docker-compose build
	docker-compose up -d
	docker exec xing-php ./bin/setup-docker.sh

.PHONY: bash
bash:
	docker exec -it xing-php /bin/bash

.PHONY: build
build: up

.PHONY: down
down:
	docker-compose down

.PHONY: clean
clean:
	docker-compose down -v

.PHONY: test
test:
	docker exec xing-php ./bin/console cache:clear --env=test
	docker exec xing-php ./bin/phpunit --bootstrap vendor/autoload.php tests

.PHONY: tests
tests:
	docker exec xing-php ./bin/phpunit --bootstrap vendor/autoload.php --configuration phpunit.xml.dist tests

.PHONY: composer-update
composer-update:
	docker exec xing-php composer update
